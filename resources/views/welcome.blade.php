<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="mover,truck and movers,house moving service,lorry for hire,family movers,lanka mover,relocation movers,professional moving companies"/>
    <meta name="author" content="Cyntrek Solutions"/>
    <meta name="robots" content="index"/>
    <meta property="description" content="Best and Lowest Moving Service in Sri Lanka. We are the outcome of a group of skilled and dynamic personnel in Transport.We Deliver Your Happiness"/>
    <meta property="og:title" content="PickMovers - Transport and Cargo Service"/>
    <meta property="og:description" content="Best and Lowest Moving Service in Sri Lanka. We are the outcome of a group of skilled and dynamic personnel in Transport.We Deliver Your Happiness"/>
    <meta property="og:image" content="social-image.png"/>
    <meta name="google-site-verification" content="xhnO9MB_QOjDDZ3MCR1P9qeeVND2Ift93RXQ27r98TA" />
    <!-- FAVICON ICON -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png"/>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-105029573-7"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-105029573-7');
    </script>
    <!-- PAGE TITLE HERE -->
    <title>Pick Movers </title>

    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width">

    <!--[if lt IE 9]>
    <script src="https://cargozone.dexignlab.com/xhtml/js/html5shiv.min.js"></script>
    <script src="https://cargozone.dexignlab.com/xhtml/js/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="{{asset('/css/plugins.css')}}">
    <link rel="stylesheet" type="text/css" href="{{mix('/css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{mix('/css/revolution.css')}}">

</head>
<body id="bg">
<div class="page-wraper">
    <div id="loading-area"></div>
    <!-- Header -->
    <header class="site-header header-style-4 style-1 mo-left onepage" id="head">
        <!-- Top Bar -->
        <div class="top-bar">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="dez-topbar-left">
                        <ul class="social-line text-center pull-right">
                            <li>
                                <a href="javascript:void(0);">
                                    <i class="fa fa-map-marker"></i>
                                    <span> 72/A2,Gramodaya Rd,Kalagoda,Pannipitiya</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="dez-topbar-right">
                        <ul class="social-line text-center pull-right">
                            <li><a href="https://www.facebook.com/Pick-Movers-102637641227305" target="_blank" class="fa fa-facebook"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top Bar END-->
        <!-- Main Header -->
        <div class="sticky-header main-bar-wraper navbar-expand-lg">
            <div class="main-bar clearfix ">
                <div class="container header-contant-block">
                    <div class="row">
                        <div class="col-xl-4 col-lg-3 d-flex">
                            <div class="logo-header align-self-center">
                                <a href="/"><img src="images/logo.webp" width="193" height="89" alt=""></a>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-9">
                            <ul class="contact-info clearfix">
                                <li>
                                    <h6 class="text-primary"><i class="fa fa-phone"></i> Call Us</h6>
                                    <a style="color: black" href="tel:+94777113515">+94 777 11 35 15</a>
                                </li>
                                <li>
                                    <h6 class="text-primary"><i class="fa fa-envelope-o"></i> Send us an Email</h6>
                                    <span>pickmovers@gmail.com</span></li>
                                <li>
                                    <h6 class="text-primary"><i class="fa fa-clock-o"></i> Opening Time</h6>
                                    <span>Mon - Sun: 24 Hours</span></li>
                                <li>
                                    <a href="tel:+94710999631"
                                       class="site-button m-r15 text-white text-center btn-block">
                                        <h5 class="m-a0">Call Now</h5>
                                        <p class="m-a0">+94 710 999 631</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="slide-up">
                    <div class="container clearfix ">
                        <!-- Website Logo -->
                        <div class="logo-header mostion">
                            <a href="/"><img src="images/logo.webp" width="193" height="89" alt=""></a>
                        </div>
                        <!-- Nav Toggle Button -->
                        <button class="navbar-toggler collapsed navicon justify-content-end" type="button"
                                data-toggle="collapse" data-target="#navbarNavDropdown"
                                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>

                        <!-- Main Nav -->
                        <div class="header-nav navbar-collapse collapse navbar justify-content-start"
                             id="navbarNavDropdown">
                            <ul class="nav navbar-nav" id="myNavbar">
                                <li><a class="scroll nav-link" href="#head">Home</a></li>
                                <li><a class="scroll nav-link" href="#section1">Services</a></li>
                                <li><a class="scroll nav-link" href="#section2">Request A Quote</a></li>
                                <li><a class="scroll nav-link" href="#section3">Team</a></li>
                                <li><a class="scroll nav-link" href="#section4">Stats</a></li>
                                <li><a class="scroll nav-link" href="#section5">About Us</a></li>
                                <li><a class="scroll nav-link" href="#section6"> Testimonials </a></li>
                                <li><a class="scroll nav-link" href="#section7">Contact Us </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Header END -->
    </header>
    <!-- Header END -->
    <!-- Content -->
    <div class="page-content bg-white">
        <!-- Slider -->
        <div class="main-slider style-two default-banner">
            <div class="tp-banner-container">
                <div class="tp-banner">
                    <div id="rev_slider_1014_1_wrapper" class="rev_slider_wrapper fullscreen-container"
                         data-alias="typewriter-effect" data-source="gallery">
                        <!-- START REVOLUTION SLIDER 5.3.0.2 -->
                        <div id="rev_slider_1014_1" class="rev_slider fullscreenbanner" style="display:none;"
                             data-version="5.3.0.2">
                            <ul>
                                <!-- SLIDE 1 -->
                                <li data-index="rs-1000" data-transition="zoomin" data-slotamount="7"
                                    data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power4.easeInOut"
                                    data-easeout="Power4.easeInOut" data-masterspeed="2000"
                                    data-thumb="images/main-slider/slide1.webp" data-rotate="0"
                                    data-saveperformance="off" data-title="Love it?" data-param1="" data-param2=""
                                    data-param3="" data-param4="" data-param5="" data-param6="" data-param7=""
                                    data-param8="" data-param9="" data-param10="" data-description="">
                                    <!-- MAIN IMAGE -->
                                    <img src="images/main-slider/slide1.webp" alt="" data-bgposition="center center"
                                         data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone"
                                         data-scalestart="100" data-scaleend="120" data-rotatestart="0"
                                         data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500"
                                         data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                    <!-- LAYER NR. 1 [ for overlay ] -->
                                    <div class="tp-caption tp-shape tp-shapewrapper "
                                         id="slide-100-layer-1"
                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                         data-width="full"
                                         data-height="full"
                                         data-whitespace="nowrap"
                                         data-type="shape"
                                         data-basealign="slide"
                                         data-responsive_offset="off"
                                         data-responsive="off"
                                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}]'
                                         data-textAlign="['left','left','left','left']"
                                         data-paddingtop="[0,0,0,0]"
                                         data-paddingright="[0,0,0,0]"
                                         data-paddingbottom="[0,0,0,0]"
                                         data-paddingleft="[0,0,0,0]"
                                         style="z-index: 12;background-color:rgba(0, 0, 0, 0.40);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>
                                    <!-- LAYER NR. 2 [ for title ] -->
                                    <div class="tp-caption  tp-resizeme"
                                         id="slide-100-layer-2"
                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                         data-y="['top','top','top','top']" data-voffset="['190','190','190','190']"
                                         data-fontsize="['50','50','50','50']"
                                         data-lineheight="['70','70','70','70']"
                                         data-width="['700','700','700','700']"
                                         data-height="['none','none','none','none']"
                                         data-whitespace="['normal','normal','normal','normal']"
                                         data-type="text"
                                         data-responsive_offset="on"
                                         data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":750,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                         data-textAlign="['center','center','center','center']"
                                         data-paddingtop="[0,0,0,0]"
                                         data-paddingright="[0,0,0,0]"
                                         data-paddingbottom="[0,0,0,0]"
                                         data-paddingleft="[0,0,0,0]"
                                         style="z-index: 13;
									font-size: 65px;
									line-height: 60px;
									font-weight: 700;
									color: rgba(255, 255, 255, 1.00);
									border-width:0px;"> <span style="font-family:'Poppins' ,sans-serif;">You Focus On Your <br>
                                        Business</span></div>
                                    <!-- LAYER NR. 3 [ for paragraph] -->
                                    <div class="tp-caption tp-resizeme"
                                         id="slide-100-layer-3"
                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                         data-y="['top','top','top','top']" data-voffset="['340','340','340','340']"
                                         data-fontsize="['20','20','20','35']"
                                         data-lineheight="['30','30','30','40']"
                                         data-width="['800','800','800','1000']"
                                         data-height="['none','none','none','none']"
                                         data-whitespace="['normal','normal','normal','normal']"
                                         data-type="text"
                                         data-responsive_offset="on"
                                         data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":750,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                         data-textAlign="['center','center','center','center']"
                                         data-paddingtop="[0,0,0,0]"
                                         data-paddingright="[0,0,0,0]"
                                         data-paddingbottom="[0,0,0,0]"
                                         data-paddingleft="[0,0,0,0]"
                                         style="z-index: 13;
									font-weight: 600;
									color: rgba(255, 255, 255, 0.85);
									border-width:0px; font-family:'Poppins' ,sans-serif;"><span>we have been providing our customers with the most reliable moving services in Sri Lanka. Our staff are well-trained experts who have years of experience and can handle your relocation professionally.</span>
                                    </div>
                                    <!-- LAYER NR. 4 [ for readmore botton ] -->
                                    <div class="tp-caption tp-resizeme"
                                         id="slide-100-layer-4"
                                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                         data-y="['top','top','top','top']" data-voffset="['450','450','450','550']"
                                         data-fontsize="['none','none','none','none']"
                                         data-lineheight="['none','none','none','none']"
                                         data-width="['700','700','700','700']"
                                         data-height="['none','none','none','none']"
                                         data-whitespace="['normal','normal','normal','normal']"
                                         data-type="text"
                                         data-responsive_offset="on"
                                         data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":750,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                                         data-textAlign="['center','center','center','center']"
                                         data-paddingtop="[0,0,0,0]"
                                         data-paddingright="[0,0,0,0]"
                                         data-paddingbottom="[0,0,0,0]"
                                         data-paddingleft="[0,0,0,0]"
                                         style="z-index: 13;"><a href="javascript:void(0);" class="site-button"><span>Read More</span></a>
                                    </div>
                                </li>

                            </ul>
                            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                        </div>
                    </div>
                    <!-- END REVOLUTION SLIDER -->
                </div>
            </div>
        </div>
        <!-- Slider END -->
        <!-- Our Service Box -->
        <div class="section-full our-service p-b10">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="icon-bx-wraper text-center service-box p-a30 bg-white skew-angle">
                            <div class="icon-lg radius"><span class="icon-cell"><i class="fa fa-truck"></i></span></div>
                            <div class="icon-content">
                                <h4 class="m-b0 m-t10">Fast Delivery Network</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="icon-bx-wraper text-center service-box p-a30 bg-white skew-angle">
                            <div class="icon-lg radius"><span class="icon-cell"><i class="fa fa-users"></i></span></div>
                            <div class="icon-content">
                                <h4 class="m-b0 m-t10">Well Qualified Staff</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="icon-bx-wraper text-center service-box p-a30 bg-white skew-angle">
                            <div class="icon-lg radius"><span class="icon-cell"><i class="fa fa-clock-o"></i></span>
                            </div>
                            <div class="icon-content">
                                <h4 class="m-b0 m-t10">24/7 Service</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="icon-bx-wraper text-center service-box p-a30 bg-white skew-angle">
                            <div class="icon-lg radius"><span class="icon-cell"><i class="fa fa-gift"></i></span></div>
                            <div class="icon-content">
                                <h4 class="m-b0 m-t10">Great Discounts</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Our Service Box End -->
        <!-- Our Service -->
        <div class="section-full awesome-services bg-white p-t60 p-b40" id="section1">
            <div class="container">
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12 text-center section-head">
                            <h3 class="h3">Our Services</h3>
                            <div class="dez-separator bg-primary"></div>
                            <div class="clear"></div>
                            <p class="m-b0">We hold our services to the highest of standards for the customer.Keeping
                                your valuables safe during your move is our top priority.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="dez-box m-b30">
                                <div class="dez-media dez-img-effect zoom"><img src="images/our-work/pic1.jpg" alt="">
                                    <div class="dez-info-has p-a20 bg-secondry no-hover">
                                        <div class="icon-bx-wraper center">
                                            <div class="icon-md text-primary m-b20"><a href="javascript:void(0);"
                                                                                       class="icon-cell"><i
                                                        class="fa fa-home"></i></a></div>
                                            <div class="icon-content">
                                                <h3 class="dez-tilte m-b5">Residential Household Moving</h3>
                                                <p>Move your household items completely or partially</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="dez-box m-b30">
                                <div class="dez-media dez-img-effect zoom"><img src="images/our-work/pic2.jpg" alt="">
                                    <div class="dez-info-has p-a20 bg-secondry no-hover">
                                        <div class="icon-bx-wraper center">
                                            <div class="icon-md text-primary m-b20"><a href="javascript:void(0);"
                                                                                       class="icon-cell"><i
                                                        class="fa fa-building"></i></a></div>
                                            <div class="icon-content">
                                                <h3 class="dez-tilte m-b5">Business Relocation</h3>
                                                <p>Relocate your business with moving your office furniture and fixing
                                                    it</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="dez-box m-b30">
                                <div class="dez-media dez-img-effect zoom"><img src="images/our-work/pic3.jpg" alt="">
                                    <div class="dez-info-has p-a20 bg-secondry no-hover">
                                        <div class="icon-bx-wraper center">
                                            <div class="icon-md text-primary m-b20"><a href="javascript:void(0);"
                                                                                       class="icon-cell"><i
                                                        class="fa fa-map-marker"></i></a></div>
                                            <div class="icon-content">
                                                <h3 class="dez-tilte m-b5">Packaging & Moving</h3>
                                                <p>Pack your items and ensure the safe when moving. You do not need to
                                                    worry</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="dez-box m-b30">
                                <div class="dez-media dez-img-effect zoom"><img src="images/our-work/pic4.jpg" alt="">
                                    <div class="dez-info-has p-a20 bg-secondry no-hover">
                                        <div class="icon-bx-wraper center">
                                            <div class="icon-md text-primary m-b20"><a href="javascript:void(0);"
                                                                                       class="icon-cell"><i
                                                        class="fa fa-cab"></i></a></div>
                                            <div class="icon-content">
                                                <h3 class="dez-tilte m-b5">Delivery & Unpacking</h3>
                                                <p>Unpack your items and reinstall in your new location as you wish</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="dez-box m-b30">
                                <div class="dez-media dez-img-effect zoom"><img src="images/our-work/pic5.jpg" alt="">
                                    <div class="dez-info-has p-a20 bg-secondry no-hover">
                                        <div class="icon-bx-wraper center">
                                            <div class="icon-md text-primary m-b20"><a href="javascript:void(0);"
                                                                                       class="icon-cell"><i
                                                        class="fa fa-building-o"></i></a></div>
                                            <div class="icon-content">
                                                <h3 class="dez-tilte m-b5">Furniture Moving & Installation</h3>
                                                <p>Specialized in transportation of furniture and assemble your
                                                    furniture and other items</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="dez-box m-b30">
                                <div class="dez-media dez-img-effect zoom"><img src="images/our-work/pic6.jpg" alt="">
                                    <div class="dez-info-has p-a20 bg-secondry no-hover">
                                        <div class="icon-bx-wraper center">
                                            <div class="icon-md text-primary m-b20"><a href="javascript:void(0);"
                                                                                       class="icon-cell"><i
                                                        class="fa fa-gift"></i></a></div>
                                            <div class="icon-content">
                                                <h3 class="dez-tilte m-b5">Transportation & Crating</h3>
                                                <p>Various packaging and crating solutions that will ensure the safe
                                                    transportation of your goods</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Our Awesome Services END -->
        <!-- Request A Quote -->
        <div class="section-full text-white bg-img-fix p-t70 p-b40 overlay-black-middle request-a-quote choose-us"
             style="background-image:url(images/background/bg2.jpg);" id="section2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="text-left">
                            <h3 class="h3">Request A Quote</h3>
                            <div class="dez-separator bg-primary"></div>
                            <p>We are dedicated to helping you relocate your office or home efficiently and with as little stress as possible.</p>
                        </div>
                        <div class="p-t10 p-b20 clearfix">
                            <form method="post" class="dzForm"
                                  action="https://cargozone.dexignlab.com/xhtml/script/contact.php">
                                <input type="hidden" value="Appoinment" name="dzToDo">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input name="dzName" class="form-control" placeholder="Name" type="text">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input name="dzEmail" class="form-control" placeholder="Email" type="text">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <input name="dzOther[date]" class="form-control" type="date">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <textarea name="dzMessage" rows="4" class="form-control"
                                                          required=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 text-center">
                                        <div class="dzFormMsg"></div>
                                        <button name="Reset" value="Reset" type="reset"
                                                class="site-button m-r5 skew-secondry">
                                            <span>Reset</span>
                                        </button>
                                        <button name="submit" type="submit" value="Submit"
                                                class="site-button skew-secondry">
                                            <span>Submit</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Request A Quote End -->
        <!-- Team member -->
        <div class="section-full bg-white p-t70 p-b40" id="section3">
            <div class="container">
                <div class="section-head text-center">
                    <h2 class="h2">Meet Our Team</h2>
                    <div class="dez-separator bg-primary"></div>
                    <p>We will assist you with packing, provide moving boxes and other moving supplies, help with unpacking,
                        storage units for your belongings, as well as regular and enclosed auto transport</p>
                </div>
                <div class="section-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 m-b30">
                            <div class="dez-box">
                                <div class="dez-media"><a href="javascript:void(0);"><img src="images/our-team/pic1.jpg"
                                                                                          alt=""></a></div>
                                <div class="dez-info p-a20 p-t40 border-1">
                                    <h4 class="dez-title m-tb0"><a href="javascript:void(0);">Jimmy Morris</a></h4>
                                    <div class="bg-primary skew-content-box">
                                    </div>
                                    <span>Senor Vice President</span>
{{--                                    <p class="m-t10 m-b0">Lorem ipsum dolor Fusce varius euismod lacus eget feugiat--}}
{{--                                        rorem ipsum dolor consectetur Fusce varius. </p>--}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 m-b30">
                            <div class="dez-box">
                                <div class="dez-media"><a href="javascript:void(0);"><img src="images/our-team/pic2.jpg"
                                                                                          alt=""></a></div>
                                <div class="dez-info p-a20 p-t40 border-1">
                                    <h4 class="dez-title m-tb0"><a href="javascript:void(0);">Jimmy Morris</a></h4>
                                    <div class="bg-primary skew-content-box">
                                    </div>
                                    <span>Senor Vice President</span>
{{--                                    <p class="m-t10 m-b0">Lorem ipsum dolor Fusce varius euismod lacus eget feugiat--}}
{{--                                        rorem ipsum dolor consectetur Fusce varius.</p>--}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 m-b30">
                            <div class="dez-box">
                                <div class="dez-media"><a href="javascript:void(0);"><img src="images/our-team/pic3.jpg"
                                                                                          alt=""></a></div>
                                <div class="dez-info p-a20 p-t40 border-1">
                                    <h4 class="dez-title m-tb0"><a href="javascript:void(0);">Jimmy Morris</a></h4>
                                    <div class="bg-primary skew-content-box">
                                    </div>
                                    <span>Senor Vice President</span>
{{--                                    <p class="m-t10 m-b0">Lorem ipsum dolor Fusce varius euismod lacus eget feugiat--}}
{{--                                        rorem ipsum dolor consectetur Fusce varius.</p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Team member -->
        <!-- Company Stats  -->
        <div class="section-full bg-img-fix p-t70 p-b40 overlay-black-dark our-projects-galery"
             style="background-image:url(images/background/bg3.jpg);" id="section4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                        <div class="icon-bx-wraper center text-white">
                            <div class="icon-xl m-b20"><span class="icon-cell"> <i class="fa fa-group"></i></span></div>
                            <div class="m-t10">
                                <div class="dez-separator bg-primary"></div>
                            </div>
                            <div class="icon-content">
                                <h2 class="dez-tilte text-uppercase h2"><span class="counter">50</span> + </h2>
                                <h6>Passionate Employees</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                        <div class="icon-bx-wraper center text-white">
                            <div class="icon-xl m-b20"><span class="icon-cell"> <i class="fa fa-university"></i></span>
                            </div>
                            <div class="m-t10">
                                <div class="dez-separator bg-primary"></div>
                            </div>
                            <div class="icon-content">
                                <h2 class="dez-tilte text-uppercase h2"><span class="counter">200</span> + </h2>
                                <h6>Local Clients</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                        <div class="icon-bx-wraper center text-white">
                            <div class="icon-xl m-b20"><span class="icon-cell"> <i class="fa fa-truck"></i></span></div>
                            <div class="m-t10">
                                <div class="dez-separator bg-primary"></div>
                            </div>
                            <div class="icon-content">
                                <h2 class="dez-tilte text-uppercase h2"><span class="counter">24</span> + </h2>
                                <h6>Cargo Trucks</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                        <div class="icon-bx-wraper center text-white">
                            <div class="icon-xl m-b20"><span class="icon-cell"> <i class="fa fa-building"></i></span>
                            </div>
                            <div class="m-t10">
                                <div class="dez-separator bg-primary"></div>
                            </div>
                            <div class="icon-content">
                                <h2 class="dez-tilte text-uppercase h2"><span class="counter">6</span> + </h2>
                                <h6>Local Offices</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Company Stats END -->

        <!-- About Company -->
        <div class="section-full  bg-white p-t70 p-b40" id="section5">
            <div class="container">
                <div class="section-content">
                    <div class="row">
                        <div class="col-lg-5 m-b30 disnone-sm text-center">
                            <div class="dez-thu"><img src="images/image2.jpg" alt=""></div>
                        </div>
                        <div class="col-lg-7">
                            <h2 class="h2 m-t0"> About Our <span class="text-primary">PickMover </span></h2>
                            <div class="dez-separator bg-primary"></div>
                            <div class="clear"></div>
                            <p class="m-b20"><strong>Working for more than two decades our network is underpinned with
                                    professionalism, standard and customization. Every service and its distinctive elements are carefully planned for
                                    customer’s satisfaction.
                                    Keeping in mind every aspect of moving; PickMovers work as a team of freight forwarders to process your move in an organized way.
                                </strong></p>
                            <p class="m-b40">We are a Sri Lanka based Moving Company recognized as Local movers. Our freight forwarder team experts will help you with moving,
                                packing and storage. We are serving global clientele and are indeed, a hassle free movers and packers with International shipping standards
                                and is why a trusted name in the
                                International market.</p>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="icon-bx-wraper bx-style-1 p-a20 left m-b30">
                                        <div class="bg-primary icon-bx-xs m-b20 "><span class="icon-cell"> <i
                                                    class="fa fa-truck"></i> </span></div>
                                        <div class="icon-content">
                                            <h5 class="dez-tilte ">Fast Delivery Network</h5>
                                            <p>Fast and va;uble service for you</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="icon-bx-wraper bx-style-1 p-a20 left m-b30">
                                        <div class="bg-primary icon-bx-xs m-b20"><span class="icon-cell"> <i
                                                    class="fa fa-users"></i> </span></div>
                                        <div class="icon-content">
                                            <h5 class="dez-tilte ">Well Qualified Staff</h5>
                                            <p>Well Experienced crew members to support</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="icon-bx-wraper bx-style-1 p-a20 left m-b30">
                                        <div class="bg-primary icon-bx-xs m-b20"><span class="icon-cell"> <i
                                                    class="fa fa-clock-o"></i> </span></div>
                                        <div class="icon-content">
                                            <h5 class="dez-tilte ">24/7 Service</h5>
                                            <p>24 hour 365 days open to serve you</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="icon-bx-wraper bx-style-1 p-a20 left m-b30">
                                        <div class="bg-primary icon-bx-xs m-b20 "><span class="icon-cell"> <i
                                                    class="fa fa-gift"></i> </span></div>
                                        <div class="icon-content">
                                            <h5 class="dez-tilte ">Great Discounts</h5>
                                            <p>Call Now and get limited offers for you</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About Company END -->
        <!-- Testimonials -->
        <div class="section-full bg-img-fix p-t70 p-b40 overlay-black-dark"
             style="background-image:url(images/background/bg3.jpg);" id="section6">
            <div class="container">
                <div class="section-head text-center text-white">
                    <h2 class="h2"> What Are Customer Saying</h2>
                    <div class="dez-separator bg-primary"></div>
                    <p>Great service make more customer happier and they reward us</p>
                </div>
                <div class="section-content">
                    <div class="testimonial-five owl-carousel owl-none">
                        <div class="item">
                            <div class="testimonial-6">
                                <div class="testimonial-text bg-white quote-left quote-right">
                                    <p>They delivered on time and nothing was damaged. Good customer service , and very responsible service.</p>
                                </div>
                                <div class="testimonial-detail clearfix bg-primary text-white">
                                    <h4 class="testimonial-name m-tb0">Piyumi</h4>
{{--                                    <span class="testimonial-position">Student</span>--}}
                                    <div class="testimonial-pic radius"><img src="images/testimonials/pic1.jpg" alt=""
                                                                             width="100" height="100"></div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-6">
                                <div class="testimonial-text bg-white quote-left quote-right">
                                    <p>The service has been provided from PickMovers was excellent.an confident t
                                        o say that the service the providing was fast and reasonable..</p>
                                </div>
                                <div class="testimonial-detail clearfix bg-primary text-white">
                                    <h4 class="testimonial-name m-tb0">Tanya</h4>
{{--                                    <span class="testimonial-position">Student</span>--}}
                                    <div class="testimonial-pic radius"><img src="images/testimonials/pic2.jpg" alt=""
                                                                             width="100" height="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonials End -->
    </div>
    <!-- Footer -->
    <div class="" id="section7">
        <div class="bg-white p-b50 p-t80">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-head text-centere text-center">
                            <h2 class="h2">Get in <span class="text-primary">Touch</span></h2>
                            <div class="dez-separator bg-primary"></div>
                            <p>Thank you for visiting the PickMovers website,
                                please contact a member of the team directly for further information or if we can assist you further in anyway.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- Left part start -->
                    <div class="col-lg-9 col-md-7">
                        <div class="dzFormMsg"></div>
                        <div class="clearfix m-b30">
                            <form method="post" class="dzForm"
                                  action="https://cargozone.dexignlab.com/xhtml/script/contact.php">
                                <input type="hidden" value="Contact" name="dzToDo">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
													<span class="input-group-text">
														<i class="fa fa-user"></i>
													</span>
                                                </div>
                                                <input name="dzName" type="text" required class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
													<span class="input-group-text">
														<i class="fa fa-envelope"></i>
													</span>
                                                </div>
                                                <input name="dzEmail" placeholder="Email" type="email"
                                                       class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend v-align-t">
													<span class="input-group-text">
														<i class="fa fa-pencil"></i>
													</span>
                                                </div>
                                                <textarea name="dzMessage" placeholder="Message" rows="4"
                                                          class="form-control" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="g-recaptcha-bx">
                                            <div class="g-recaptcha"
                                                 data-sitekey="6LefsVUUAAAAADBPsLZzsNnETChealv6PYGzv3ZN"
                                                 data-callback="verifyRecaptchaCallback"
                                                 data-expired-callback="expiredRecaptchaCallback"></div>
                                            <input class="form-control d-none" style="display:none;"
                                                   data-recaptcha="true" required
                                                   data-error="Please complete the Captcha">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <button name="submit" type="submit" value="Submit" class="site-button">
                                            <span>Submit</span>
                                        </button>
                                        <button name="Resat" type="reset" value="Reset" class="site-button m-l15">
                                            <span>Reset</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Left part END -->
                    <!-- right part start -->
                    <div class="col-lg-3 col-md-5">
                        <div class="bg-white m-b30">
                            <ul class="no-margin">
                                <li class="icon-bx-wraper left m-b30">
                                    <div class="icon-bx-xs bg-primary"><a href="javascript:void(0);"
                                                                          class="icon-cell"><i
                                                class="fa fa-map-marker"></i></a></div>
                                    <div class="icon-content">
                                        <h6 class="text-uppercase m-b0 dez-tilte">Address</h6>
                                        <p> 72/A2<br>Gramodaya Rd<br>Kalagoda<br>Pannipitiya</p>
                                    </div>
                                </li>
                                <li class="icon-bx-wraper left  m-b30">
                                    <div class="icon-bx-xs bg-primary"><a href="javascript:void(0);"
                                                                          class="icon-cell"><i
                                                class="fa fa-envelope"></i></a></div>
                                    <div class="icon-content">
                                        <h6 class="text-uppercase m-b0 dez-tilte">EMAIl</h6>
                                        <p>pickmovers@gmail.com</p>
                                    </div>
                                </li>
                                <li class="icon-bx-wraper left">
                                    <div class="icon-bx-xs bg-primary"><a href="javascript:void(0);"
                                                                          class="icon-cell"><i class="fa fa-phone"></i></a>
                                    </div>
                                    <div class="icon-content">
                                        <h6 class="text-uppercase m-b0 dez-tilte">PHONE</h6>
                                        <a href="tel:+94777113515" style="color: black">+94 777 11 35 15</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- right part END -->
                </div>
            </div>
        </div>
        <!-- footer bottom part -->
        <div class="footer-bottom bg-primary">
            <div class="container">
                <div class="row text-white">
                    <div class="col-lg-4 text-left">
                        <span>© Copyright {{\Carbon\Carbon::now()->year}}</span>
                    </div>
                    <div class="col-lg-4 text-center">
                        <span>Powered  <i class="fa fa-heart heart"> </i> By Cyntrek Solutions </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer END-->
    <!-- scroll top button -->
    <button class="scroltop fa fa-arrow-up"></button>
</div>
<!-- JavaScript files ========================================= -->
<script src="js/jquery.min.js"></script><!-- JQUERY.MIN JS -->
<script src="plugins/bootstrap/js/popper.min.js"></script><!-- BOOTSTRAP.MIN JS -->
<script src="plugins/bootstrap/js/bootstrap.min.js"></script><!-- BOOTSTRAP.MIN JS -->
<script src="plugins/bootstrap-select/bootstrap-select.min.js"></script><!-- FORM JS -->
<script src="plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script><!-- FORM JS -->
<script src="plugins/magnific-popup/magnific-popup.js"></script><!-- MAGNIFIC POPUP JS -->
<script src="plugins/counter/waypoints-min.js"></script><!-- WAYPOINTS JS -->
<script src="plugins/counter/counterup.min.js"></script><!-- COUNTERUP JS -->
<script src="plugins/imagesloaded/imagesloaded.js"></script><!-- IMAGESLOADED -->
<script src="plugins/masonry/masonry-3.1.4.js"></script><!-- MASONRY -->
<script src="plugins/masonry/masonry.filter.js"></script><!-- MASONRY -->
<script src="plugins/owl-carousel/owl.carousel.js"></script><!-- OWL SLIDER -->
{{--<script src="plugins/switcher/js/switcher.js"></script><!-- SWITCHER JS  -->--}}
<script src="js/custom.js"></script><!-- CUSTOM FUCTIONS  -->
<script src="js/dz.carousel.js"></script><!-- SORTCODE FUCTIONS  -->
<script src="js/dz.ajax.js"></script><!-- CONTACT JS  -->
{{--<script src='../../www.google.com/recaptcha/api.js'></script> <!-- Google API For Recaptcha  -->--}}
<!-- REVOLUTION JS FILES -->
<script src="plugins/revolution/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js"></script>
<!-- Slider revolution 5.0 Extensions  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="plugins/revolution/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="plugins/revolution/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="plugins/revolution/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="plugins/revolution/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="plugins/revolution/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="plugins/revolution/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="plugins/revolution/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="plugins/revolution/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="plugins/revolution/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script>
    var tpj = jQuery;

    var revapi1014;
    tpj(document).ready(function () {
        if (tpj("#rev_slider_1014_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_1014_1");
        } else {
            revapi1014 = tpj("#rev_slider_1014_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "//server.local/revslider/wp-content/plugins/revslider/public/assets/js/",
                dottedOverlay: "none",
                delay: 9000,
                startwidth: 1200,
                startheight: 750,
                hideThumbs: 600,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    mouseScrollReverse: "default",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    }
                },
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],

                lazyType: "none",
                shadow: 0,
                spinner: "off",
                stopLoop: "on",
                stopAfterLoops: 0,
                stopAtSlide: 1,
                shuffle: "off",
                autoHeight: "off",
                fullScreenAutoWidth: "off",
                fullScreenAlignForce: "off",
                fullScreenOffsetContainer: "",
                fullScreenOffset: "60px",
                disableProgressBar: "on",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }
    });
</script>
</body>
</html>
