const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'resources/css/style.css',
    'resources/css/templete.css',
    'resources/css/skin/skin-1.css',
],'public/css/app.css');

mix.styles([
    'resources/css/settings.css',
    'resources/css/navigation.css',
],'public/css/revolution.css');

mix.styles('resources/css/plugins.css','public/css/plugins.css');
